<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribuicao extends Model
{
    protected  $fillable = [

    'id',
    'idUsuarioContribuinte',
    'dataContribuicao',
    'idTipoContribuicao',
    'valor',
    'idFormaContribuicao',
    'observacao',
        
    ];

    protected $table = 'contribuicao';

         public function departamento()
            {
            return $this->belongsTo(Departamentos::class, 'idDepartamento');
            }

        public function tipoContribuicao()
         {
         return $this->belongsTo(TipoContribuicaos::class,'idTipoContribuicao');
         }

     public function contribuinte()
      {
       return $this->belongsTo(Contribuintes::class, 'idContribuinte');
      }

}
