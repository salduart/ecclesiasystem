<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaCampo extends Model
{
    protected  $fillable = [
    'id',
    'nome'
  ];

  protected $table = 'areaCampo';
}
