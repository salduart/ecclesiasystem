<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fone extends Model
{
    protected  $fillable = [
    'id',
    'numero'
    'idTipoFone'
  ];

  protected $table = 'fone';

  public function tipoFone()
    {
        return $this->belongsTo(TipoFones::class, 'idTipoFone');
    }
}
