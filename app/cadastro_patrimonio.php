<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cadastroPatrimonio extends Model
{
     protected  $fillable = [
    'id'
    'descricaoBem'
    'dataRegistro'
    'idCategoria'
    'notaFiscal'
    'numeroPatrimonial'
    'valor'
    'quantidade'
    'formaAquisicao'
    'idUsuarioDoador'
    'email'
    'observacao'
    ];
    
     protected $table = 'cadastroPatrimonio';
    
     public function status()
       
        {
          return $this->belongsTo(Categorias::class, 'idCategoria');
        }
    
    public function status()
     
     {
     	return $this->belongsTo(UsuarioDoadors::class,'idUsuarioDoador');
     	
     }
}
