<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class saidaContribuicao extends Model
{
     protected  $fillable = [
    'id',
    'idUsuarioAutorizacao'
    'destino'
    'valor'
    'observacao'
    'data'
    ];
    
    protected $table = 'saidaContibuicao';
    
     public function status()
        {
          return $this->belongsTo(UsuarioAutorizacaos::class, 'idUsuarioAutorizacao');
        }
}
