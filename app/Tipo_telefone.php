<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_telefone extends Model
{
    protected  $fillable = [
    'id',
    'tipo'
  ];

  protected $table = 'tipoTelefone';
}
