<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cadastroEventos extends Model
{
   protected  $fillable = [
    'id'
    'descricaoEvento'
    'dataEvento'
    'localEvento'
    'idUsuarioResponsavel'
    'dataAutorizacao'
    'idUsuarioAutorizacao'
    
    ];
    
    protected $table = 'cadastroEventos';
    
     public function status()
       
        {
          return $this->belongsTo(UsuarioResponsavels::class, 'idUsuarioResponsavel');
        }
    
    public function status()
     
     {
     	return $this->belongsTo(UsuarioAutorizacaos::class,'idUsuarioAtorizacao');
     	
     }
}
