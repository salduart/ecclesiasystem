<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected  $fillable = [
    'id',
    'rua'
    'numero'
    'bairro'
    'cep'
    'complemento'
    'idCidade'
  ];

  protected $table = 'endereco';

  public function cidade()
    {
        return $this->belongsTo(Cidades::class, 'idCidade');
    }
}
