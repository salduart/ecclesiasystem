<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected  $fillable = [
    'id',
    'idStatus',
    'idPerfil',
    'nomeRazaoSocial',
    'idEdereco',
    'cpfCnpj',
    'idFone',
    'email',
    'idCargo',
    'idEstodCivil',
    'dataNascimento',
    'idProfissao'
    'dataConversao',
    'idCongregacao',
    'dataBatismo',
    'observacao'
    ];

    protected $table = 'usuario';
    
     public function status()
        {
          return $this->belongsTo(Statuss::class, 'idStatus');
        }

        public function perfil()
        {
         return $this->belongsTo(Perfils::class, 'idPerfil');
         }

            public function endereco()
            {
             return $this->belongsTo(Enderecos::class, 'idEndereco');
             }

                public function fone()
                 {
                  return $this->belongsTo(Fones::class, 'idFone');
                  }

                    public function cargo()
                     {
                      return $this->belongsTo(Cargos::class, 'idCargo');
                      }

         	    public function estadoCivil()
                     {
                      return $this->belongsTo(EstadoCivils::class, 'idEstadoCivil');
                     }

              public function profissao()
               {
                return $this->belongsTo(Profissaos::class, 'idProfissao');
               }

          public function congregacao()
           {
            return $this->belongsTo(Congregacaos::class, 'idCongregacao');
           }

     

}
