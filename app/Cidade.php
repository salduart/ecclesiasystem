<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected  $fillable = [
    'id',
    'nome'
    'id_uf'
  ];

  protected $table = 'cidade';

  public function uf()
    {
        return $this->belongsTo(Ufs::class, 'idUf');
    }
}
