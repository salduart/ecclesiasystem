<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoContribuicao extends Model
{
    protected  $fillable = [
    'id',
    'nome'
  ];

  protected $table = 'tipoContribuicao';
}
