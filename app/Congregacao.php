<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Congregacao extends Model
{
     protected  $fillable = [
            'id',
            'nome',
            'idAreaCampo',
            'idDepartamento',
           
        ];
         protected $table = 'congregacao';
               
                    public function areaCampo()
                    {
                    return $this->belongsTo(AreaCampos::class, 'idAreaCampo');
                    }
                         public function departamento()
                          {
                           return $this->belongsTo(Departamentos::class, 'departamentos');
                          }
}
