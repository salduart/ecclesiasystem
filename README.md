# README #

Ecclesia System

O Ecclesia System é um software para gestão de eventos, agenda de cultos, tombamento de
patrimônio, controle financeiro, cadastro de membros etc.
No sistema é possível as seguintes ações:
• Cadastrar membro
• Cadastrar congregação
• Cadastrar patrimônio
• Cadastrar agenda de cultos por congregação
• Cadastrar eventos
• Cadastrar contribuições
• Registrar contribuinte
• Dá baixa em patrimônio
• Dá saída em contribuição
• Emitir relatórios
• Emitir recibo para saída de contribuição
• Verificar status de membro (ativos, afastado e desligado)