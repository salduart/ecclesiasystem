<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaFone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Função cria a tabela fone
                Schema::create('fone',function(Blueprint $table){
                
                $table->increments('id');
                
                $table->String('numero');
                
                $table->unsignedInteger('idTipoFone');
                $table->foreign('idTipoFone')->references('id')->on('tipoFone')->onDelete('cascade');
                
                $table->timestamps(); //serve para registrar o momento exato da alteração;
                }
                );






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Função para apagar a tabela: fone
                Schema::DropIFEXIST('fone');
    }
}
