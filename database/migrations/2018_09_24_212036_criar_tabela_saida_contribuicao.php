<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaSaidaContribuicao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saidaContribuicao', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->unsignedInteger('idUsuarioAutorizacao');
            $table->foreign('idUsuarioAutorizacao')->references('id')->on('usuario')->onDelete('cascade');
            
            $table->string('destino');
            
            $table->float('valor');
            
            $table->string('observacao');
            
            $table->dateTime('data');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saidaContribuicao');
    }
}
