<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            
            $table->increments('id');
           
            $table->unsignedInteger('idStatus');
            $table->foreign('idStatus')->references('id')->on('status')->onDelete('cascade');
            
            $table->unsignedInteger('idPerfil');
            $table->foreign('idPerfil')->references('id')->on('perfil')->onDelete('cascade');
            
            $table->string('nameRazaoSocial');
            
            $table->unsignedInteger('idEndereco');
            $table->foreign('idEndereco')->references('id')->on('endereco')->onDelete('cascade');
            
            $table->string('cpfCnpj',100);
            
            $table->unsignedInteger('idFone');
            $table->foreign('idFone')->references('id')->on('fone')->onDelete('cascade');
            
            $table->string('email')->unique();
        
            $table->unsignedInteger('idCargo');
            $table->foreign('idCargo')->references('id')->on('cargo')->onDelete('cascade');
            
            $table->unsignedInteger('idEstadoCivil');
            $table->foreign('idEstadoCivil')->references('id')->on('estadoCivil')->onDelete('cascade');
            
            $table->date('dataNascimento');
            
            $table->unsignedInteger('idProfissao');
            $table->foreign('idProfissao')->references('id')->on('profissao')->onDelete('cascade');
            
            $table->date('dateConversao');
                        
            $table->unsignedInteger('idCongregacao');
            $table->foreign('idcongregacao')->references('id')->on('congregacao')->onDelete('cascade');
            
            $table->date('dataBatismo');
            
            $table->string('observacao');
            
            $table->string('password');
            
            $table->rememberToken();
            
            $table->timestamps();
            
            $table->unique([DB::raw('email(191)')]);
                        
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
