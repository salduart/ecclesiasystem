<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaCadastroDeEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadastroEventos', function (Blueprint $table) {
            
            $table->increments('id');

            $table->string('descricaoEvento');
            
            $table->datetime('dataEvento');
            
            $table->string('localEvento');
                       
            $table->unsignedInteger('idUsuarioResponsavel');
            $table->foreign('idUsuarioResponsavel')->references('id')->on('usuario')->onDelete('cascade');
            
            $table->datetime('dataAutorizacao');

            $table->unsignedInteger('idUsuarioAutoriza');
            $table->foreign('idUsuarioAutoriza')->references('id')->on('usuario')->onDelete('cascade');

           
            
           
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cadastro_de_eventos');
    }
}
