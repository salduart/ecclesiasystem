<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaCongregacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congregacao', function (Blueprint $table) {
           
            $table->increments('id');
           
            $table->string('nome');
           
            $table->unsignedInteger('idAreaCampo');
            $table->foreign('idAreaCampo')->references('id')->on('areaCampo')->onDelete('cascade');
            
            $table->unsignedInteger('idDepartamento');
            $table->foreign('idDepartamento')->references('id')->on('departamento')->onDelete('cascade');
                                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congregacao');
    }
}
