<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaTipoTelefone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

//Função cria tabela_tipo_telefone

                Schema::create('tipoFone',function(Blueprint $table){
                
                $table->increments('id');
                
                $table->String('tipo');
                
                $table->timestamps();//serve registra o momento exato da alteração de registro
                }
                );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Função para apagar a tabela: tipo_telefone
                       Schema::DropIFEXIST('tipoFone');

    }
}
