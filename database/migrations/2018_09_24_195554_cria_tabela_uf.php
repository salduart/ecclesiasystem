<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaUf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Essa função cria Tabela UF

                        Schema::create('uf',function(Blueprint $table){
                        
                        $table->increments('id');
                        
                        $table->String('nome');
                        
                        $table->timestamps();//serve registra o momento exato da alteração de registro
                        }
                        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Função para apagar a tabela: UF
                      Schema::DropIFEXIST('uf');
    }
}
