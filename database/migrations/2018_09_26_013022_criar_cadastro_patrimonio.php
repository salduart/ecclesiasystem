<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarCadastroPatrimonio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadastroPatrimonio', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->string('descricaoBem');
            
            $table->datetime('dataRegistro');
            
            $table->unsignedInteger('idCategoria');
            $table->foreign('idCategoria')->references('id')->on('categoria')->onDelete('cascade');
            
            $table->string('notaFiscal');
            
            $table->string('numeroPatrimonial');
            
            $table->float('valor');
            
            $table->integer('quantidade');
            
            $table->string('formaAquisicao');
            
            $table->unsignedInteger('idUsuarioDoador');
            $table->foreign('idUsuarioDoador')->references('id')->on('usuario')->onDelete('cascade');
            
            $table->string('email');
            
            $table->string('observacao');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cadastroPatrimonio');
    }
}
