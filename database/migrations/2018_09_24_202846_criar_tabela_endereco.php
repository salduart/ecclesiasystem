<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaEndereco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco', function (Blueprint $table) {
           
            $table->increments('id');
           
            $table->string('rua', 150);
           
            $table->string('numero', 20);
           
            $table->string('bairro', 150);
           
            $table->string('cep', 10);
           
            $table->string('complemento', 150);
            
            $table->integer('idCidade')->unsigned();
            $table->foreign('idCidade')->references('id')->on('cidade')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endereco');
    }
}
