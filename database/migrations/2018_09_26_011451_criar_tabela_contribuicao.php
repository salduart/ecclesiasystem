<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaContribuicao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contribuicao', function (Blueprint $table) {
            
            $table->increments('id');

            $table->unsignedInteger('idUsuarioContribuinte');
            $table->foreign('idUsuarioContribuinte')->references('id')->on('usuario')->onDelete('cascade');
                        
            $table->date('dataContribuicao');
            
            $table->unsignedInteger('idTipoContribuicao');
            $table->foreign('idTipoContribuicao')->references('id')->on('tipoContribuicao')->onDelete('cascade');
            
            $table->float('valor'); 
            
            $table->unsignedInteger('idFormaContribuicao');
            $table->foreign('idFormaContribuicao')->references('id')->on('formaContribuicao')->onDelete('cascade');           
            
            $table->string('observacao');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contribuicao');
    }
}
